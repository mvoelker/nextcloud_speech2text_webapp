# Purpose
This is a webapp to record the voice and translate it into text via the web speech api [Web Api](https://developer.mozilla.org/en-US/docs/Web/API/Web_Speech_API/Using_the_Web_Speech_API). After the translation it is possible to send the text to your nextcloud instance via webdav.

# Precondidtions
- It is needed to install the [WebAppPassword](https://apps.nextcloud.com/apps/webapppassword) to allow the file upload to your instance. If you are using the script in the next point you have to entry "localhost:5173" to this WebAppPassword in the nextcloud. 

- For convience reason it is a good idea to install docker and use it via the **build_and_serve.sh**.

- Othewise you need node 16 to build this app and a webserver (apache or nginx) to serve the application. 
https://cloud.google.com/speech-to-text/docs/speech-to-text-supported-languages

- The api is only tested on chrome on desktop and mobile

- It is possible to add this webapp to you nextcloud instance via the [External Websites](https://github.com/nextcloud/external). But it need to create a subdomain and for security reasons it make sense to add a password to access the website. I am using nginx for this. I recommend the nginx proxy manager to handle this, also an automatically SSL certificate are included - via Let's Encrypt [NPM Website](https://nginxproxymanager.com/). Otherwise you have to use a apache or nginx specific configuration - [nginx configuration](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/). To avoid the permanent authentication process you could add the credentials to the url, like this: **https://\<user>:\<pass>@s2t.<your_domain>.xyz**. This is a very simple and secure way to authenticate.

# Configuration
## .env file
The addess of your instance
- VITE_NEXTCLOUD_WEBDAV_URL=https://<your_url.xyz>/remote.php/dav/files/\<username>/

Your username
- VITE_USER=\<username>

Your password
- VITE_PASSWORD=\<password>

Your language, take a look on this [Speech Api Languages](https://cloud.google.com/speech-to-text/docs/speech-to-text-supported-languages)

- VITE_SPEECH_LANG=\<your_lang>

# Workflow

For now the recording stops after a certin time there is not recorded sound. If you would like to recording the next spoken word press again record. If you are finished press the stop button and correct the text, if needed. Afterwards press save. 

![Screenshot webapp](readme_assets/screenshot_webapp.png)

A modal will be shown, there you have to write down the filename  and press upload. If the file is not present after the upload take a look into the browser console. 
 
 ![Screenshot webapp save modal](readme_assets/screenshot_webapp_modal.png)



# Todo
* comment the source code
* add more information to readme

# Thanks
Special thanks go out Dr. Robert Theurer. This was his idea and the implementation was done for him. He believes in open source and so he agreed to open source this little webapp to help other people. 
