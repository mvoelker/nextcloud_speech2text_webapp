docker rm -f stt-nginx
docker run -v $(pwd):/usr/src/app -w /usr/src/app node:16 npm i && npm run build
docker run --name stt-nginx -p 5173:80 -v $(pwd)/dist:/usr/share/nginx/html:ro -d nginx
